﻿using Foundation;
using SceneKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using UIKit;

namespace XamarinARKit.Geometry
{
    public static class MatrixHelper
    {
        /*
            11 12 13 14
            21 22 23 24
            31 32 33 34
            41 42 43 44

            Radians are used. 
         */
        public static SCNMatrix4 SetXAxis(this SCNMatrix4 m, SCNVector3 v)
        {

            return new SCNMatrix4(
                v.X, v.Y, v.Z, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44
                );
            // This is not applied to matrix.
            // m.M11 = v.X;
            // m.M12 = v.Y;
            // m.M13 = v.Z;
        }
        public static SCNMatrix4 SetYAxis(this SCNMatrix4 m, SCNVector3 v)
        {
            return new SCNMatrix4(
                m.M11, m.M12, m.M13, m.M14,
                v.X, v.Y, v.Z, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44
                );
            //m.M21 = v.X;
            //m.M22 = v.Y;
            //m.M23 = v.Z;
        }
        public static SCNMatrix4 SetZAxis(this SCNMatrix4 m, SCNVector3 v)
        {
            return new SCNMatrix4(
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                v.X, v.Y, v.Z, m.M24,
                m.M41, m.M42, m.M43, m.M44
                );
            // m.M31 = v.X;
            // m.M32 = v.Y;
            // m.M33 = v.Z;
        }
        public static SCNMatrix4 CreateWorld(Vector3 x, Vector3 y, Vector3 z, Vector3 translate)
        {
            var t = translate;
            SCNMatrix4 m = new SCNMatrix4(
                x.X, x.Y, x.Z, 0,
                y.X, y.Y, y.Z, 0,
                z.X, z.Y, z.Z, 0,
                t.X, t.Y, t.Z, 1
                );
            return m;
        }
        public static SCNMatrix4 SetTranslation(this SCNMatrix4 m, SCNVector3 v)
        {
            return new SCNMatrix4(
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                v.X, v.Y, v.Z, m.M44
                );
            //m.M41 = v.X;
            //m.M42 = v.Y;
            //m.M43 = v.Z;
        }

        public static SCNMatrix4 Convert(this Matrix4x4 m)
        {
            return new SCNMatrix4(
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44
                );
        }

        public static SCNMatrix4 Convert(this OpenTK.NMatrix4 m)
        {
            return new SCNMatrix4(
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44
                );
        }

        public static Matrix4x4 ConvertTo(this SCNMatrix4 m)
        {
            return new Matrix4x4(
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44
                );
        }

        public static Vector3 GetXAxis(this SCNMatrix4 m) => new Vector3(m.M11, m.M12, m.M13);
        public static Vector3 GetYAxis(this SCNMatrix4 m) => new Vector3(m.M21, m.M22, m.M23);
        public static Vector3 GetZAxis(this SCNMatrix4 m) => new Vector3(m.M31, m.M32, m.M33);
        public static Vector3 GetTranslation(this SCNMatrix4 m) => new Vector3(m.M41, m.M42, m.M43);


        public static SCNVector3 Convert(this Vector3 v) => new SCNVector3(v.X, v.Y, v.Z);
    }
}