﻿using ARKit;
using ARTest;
using ARTest.Models;
using ARTest.Services.Sensors;
using AudioToolbox;
using AVFoundation;
using CoreGraphics;
using Foundation;
using OpenTK;
using SceneKit;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Essentials;
using XamarinARKit.Geometry;
using XamarinARKit.Models;
using XamarinARKit.Shared.Services.Sensors;

namespace XamarinARKit
{
    public class MyLocation
    {
        public Location Location { get; set; }
        public string Name { get; set; } 
        public SCNNode TextNode { get; set; }
        public float DistanceScale { get; set; } = 1;
        public MyLocation(double latitude, double longitude, string name = "default")
        {
            Location = new Location((float)latitude, (float)longitude);
            Name = name;
        }
    }
    public partial class ViewController : UIViewController
    {
        private readonly ARSCNView sceneView;
        private readonly SCNView regularView;
        private AutoPilot pilot = new AutoPilot();
        private SCNNode aircraft;
        private List<MyLocation> locations = new List<MyLocation>();
        private List<SCNNode> objects = new List<SCNNode>();
        private UILabel label;
        private string labelText; // Put text here, we decide when to display.

        private const int refreshRate = 25; // ms
        private const int labelRefreshRate = 300; // ms
        private int currentLabelRate = 0;
        public ViewController(IntPtr handle) : base(handle)
        {
            // Lesson: Create Blank Scene
            this.sceneView = new ARSCNView()
            {
                AutoenablesDefaultLighting = true,
                DebugOptions = ARSCNDebugOptions.ShowWorldOrigin | ARSCNDebugOptions.ShowFeaturePoints,
                ShowsStatistics = true,
                
            };
            this.View.AddSubview(this.sceneView);
            regularView = new SCNView(sceneView.Frame);
            regularView.BackgroundColor = UIColor.FromRGBA(100, 20, 20, 20);
            {
                var scene = SCNScene.Create();
                regularView.Scene = scene;
                var light = SCNLight.Create();
                var lightNode = SCNNode.Create();
                light.LightType = SCNLightType.Ambient;
                light.Color = UIColor.Blue;
                lightNode.Light = light;
                scene.RootNode.AddChildNode(lightNode);

                var camera = new SCNCamera()
                {
                    XFov = 45,
                    YFov = 45,
                };
                camera.ZFar = 30000f;
                var cameraNode = new SCNNode
                {
                    Camera = camera,
                    Position = new SCNVector3(0, 0, -1)
                };
                scene.RootNode.AddChildNode(cameraNode);

            }
            View.AddSubview(regularView);

            label = new UILabel(new CGRect(new CGPoint(5,5), new CGSize(230, 400)));
            var s = "This is test text for label\nAnd in 2 rows";
            var size = new NSString(s).GetSizeUsingAttributes(new UIStringAttributes());
            label.Text = s;
            label.Font = label.Font.WithSize(10);
            label.Lines = 50;
            label.TextColor = UIColor.Cyan;
            label.BackgroundColor = UIColor.FromRGBA(0, 255, 0, 40);
            //label.Frame = new CGRect(10,10,size.Width, size.Height);
            //label.SizeToFit();

            sceneView.AddSubviews(label);

            locations.AddRange(new MyLocation[]
            {
                new MyLocation(44.8267485, 20.4944297, "Pancevac"), // Pancevac
                new MyLocation(44.8736746,20.6486263, "Pijaca"), // Pijaca
                new MyLocation(44.8251251, 20.4091021, "Stanko"), // Stanko
                new MyLocation(44.7647768, 20.4193680, "Bojan"), // Bojan
                new MyLocation(44.8719249,20.6526945, "Miladin"), // Miladin
                new MyLocation(44.8199915,20.4219336, "Office"), // Office
            });
            foreach (var ml in locations)
                ml.DistanceScale = 1f;


            var textMaterial = new SCNMaterial();
            var text = SCNText.Create("test me", 0.1f);
            if (text.FirstMaterial == null)
                text.FirstMaterial = textMaterial;
            text.FirstMaterial.Diffuse.Contents = UIColor.Blue;
            text.FirstMaterial.Specular.Contents = UIColor.Green;
            text.Font = UIFont.FromName("Courier New", 0.5f);
            text.ChamferRadius = 0.1f;
            text.Flatness = 0.1f;
            var textNode = new SCNNode();
            textNode.Geometry = text;
            textNode.Position = new SCNVector3(0, 0, -4);
            
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            var fr = View.Frame;
            this.sceneView.Frame = this.View.Frame;
            regularView.Frame = new CGRect(10, 10, fr.Width - 20, fr.Height - 20);

            var mapButton = new UIButton(UIButtonType.System);
            var rFr = regularView.Frame;
            mapButton.Frame = new CGRect(rFr.Width - 60, 10, 60, 25);
            mapButton.BackgroundColor = UIColor.Blue;
            mapButton.SetTitleColor(UIColor.White, UIControlState.Normal);
            mapButton.TouchUpInside += MapButton_TouchUpInside;
            mapButton.SetTitle("Map", UIControlState.Normal);
            regularView.AddSubview(mapButton);
            sound();

            OrientationMonitor.StartTracking();
            LocationMonitor.StartTracking();

            Action<SCNNode> transform = null;
            SCNNode model;
            float scaleFactor = 1f;
            SCNVector3 scale = new SCNVector3(scaleFactor, scaleFactor, scaleFactor);
            SCNVector3 start = new SCNVector3(-2f, 0f, -1f);
            SCNVector3 offset = new SCNVector3(1, 0, 0);
            ObjectNode n = new ObjectNode("art.scnassets/aircraft.dae");
            transform = (m) =>
            {
                m.Position = SCNVector3.Add(start, offset);
                m.Scale = scale;
            };
            foreach (var l in locations)
            { 
                model = n.CreateInstance(transform);
                //sceneView.Scene.RootNode.AddChildNode(model);
                regularView.Scene.RootNode.AddChildNode(model);
                objects.Add(model);

                {
                    // Create text over airplane
                    var textMaterial = new SCNMaterial();
                    var text = SCNText.Create("L: " + l.Name, 0.1f);
                    text.FirstMaterial = textMaterial;
                    text.FirstMaterial.Diffuse.Contents = UIColor.Blue;
                    text.FirstMaterial.Specular.Contents = UIColor.Green;
                    text.Font = UIFont.FromName("Courier New", 20f);
                    text.ChamferRadius = 0.1f;
                    text.Flatness = 0.1f;
                    var textNode = new SCNNode();
                    textNode.Geometry = text;
                    textNode.Position = new SCNVector3();
                    l.TextNode = textNode;
                    regularView.Scene.RootNode.AddChildNode(textNode);
                }
            }

            var lineMaterial = new SCNMaterial();
            lineMaterial.Diffuse.Contents = UIColor.Red;
            var line = SCNBox.Create(0.5f, 0.05f, 0.05f, 0f);
            line.FirstMaterial = lineMaterial;
            regularView.Scene.RootNode.AddChildNode(new SCNNode() { Geometry = line, Position = new SCNVector3(0, 0, -1) });
            
            lineMaterial = new SCNMaterial();
            lineMaterial.Diffuse.Contents = UIColor.Green;
            line = SCNBox.Create(0.05f, 0.1f, 0.05f, 0f);
            line.FirstMaterial = lineMaterial;
            regularView.Scene.RootNode.AddChildNode(new SCNNode() { Geometry = line, Position = new SCNVector3(0, 0, -1) });


            run();
        }

        private async void MapButton_TouchUpInside(object sender, EventArgs e)
        {
            var l = LocationMonitor.LastLocation;
            try
            {
                await Map.OpenAsync(l);
            }
            catch
            {

            }
        }

        private async void run()
        {
            pilot.Aircraft.Speed = 1;
            bool updateLabel = false;
            if ((currentLabelRate += refreshRate) > labelRefreshRate)
            {
                currentLabelRate = 0;
                updateLabel = true;
            }

            bool initial = true;
            StringBuilder builder = updateLabel ? null: new StringBuilder();
            while (true)
            {
                await Task.Delay(refreshRate);
                builder?.Clear();
                ARFrame frame = null;
                //frame = sceneView.Session.CurrentFrame;
                //await Task.Run(()=> frame = sceneView.Session.CurrentFrame);
                //if (frame != null)
                {
                    //frame = sceneView.Session.CurrentFrame;
                    //var m = frame.Camera.Transform.Convert().ConvertTo();

                    builder?.AppendLine("-----------------");
                    float north = OrientationMonitor.MagneticNorthDeg;
                    builder?.AppendLine($"North: {north:0.00}");
                    for (int i = 0; i < locations.Count; i++)
                    {
                        var location = locations[i];
                        var position = CoordinateConverter.CalculateARPositionOfTarget(location, builder);//, m);
                        var node = objects[i];
                        var p = node.Position;
                        if (p.Length < 1000)
                            p = SCNVector3.Add(p, new SCNVector3(0, 0, 20));
                        node.Position = position.Convert();
                        location.TextNode.Position = new SCNVector3(p.X, p.Y, p.Z + 0);

                        if (initial)
                        {

                            var scale = node.Scale.X;
                            var length = position.Length();
                            if (length > 1000f)
                                length = 1000f;
                            scale = scale * 30 * length / 1000f + 0.1f;
                            node.Scale = new SCNVector3(scale, scale, scale);
                        }
                    }

                }
                if (aircraft != null)
                {
                    pilot.MakeStep();
                    var scnM = pilot.Aircraft.Transform.ToMatrix().Convert();
                    aircraft.Transform = scnM;
                }
                //var mat = ARTest.MatrixHelper.GetIdentity();
                //mat.SetTranslation(new ARTest.Geometry.AVector3(0, 0, 0));
                //aircraft.Transform = XamarinARKit.Geometry.MatrixHelper.Convert(mat.ToMatrix());

                if (builder != null)
                    label.Text = builder.ToString();

                initial = false;
            }
        }
        private void sound()
        {
            uint[] n = new uint[]
            {
                1002,
                1200,
                1201,
                1202,
                1203,
                1204,
                1205,
            };

            Task.Run(async () =>
            {
                for (int i = 0; i < n.Length; i++)
                {
                    await Task.Delay(300);
                    var sound = new SystemSound(n[i]);
                    sound.PlaySystemSound();
                }
            });
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);

            this.sceneView.Session.Run(new ARWorldTrackingConfiguration
            {
                AutoFocusEnabled = true,
                LightEstimationEnabled = true,
                WorldAlignment = ARWorldAlignment.Camera,
            }, ARSessionRunOptions.ResetTracking | ARSessionRunOptions.RemoveExistingAnchors);

            var size = 0.02f;
            var cubeNode = new CubeNode(size, UIColor.Green);
            sceneView.Scene.RootNode.AddChildNode(cubeNode);
        }

        public override void ViewDidDisappear(bool animated)
        {
            base.ViewDidDisappear(animated);

            this.sceneView.Session.Pause();
        }

        public override void DidReceiveMemoryWarning()
        {
            base.DidReceiveMemoryWarning();
        }

        public override void TouchesEnded(NSSet touches, UIEvent evt)
        {
            base.TouchesEnded(touches, evt);   
            if (touches.AnyObject is UITouch touch)
            {
                var point = touch.LocationInView(sceneView);
                var hitTestOptions = new SCNHitTestOptions();

                var hits = this.sceneView.HitTest(point, hitTestOptions);
                var hit = hits.FirstOrDefault();

                if (hit == null)
                    return;

                var node = hit.Node;

                if (node != null)
                {
                    var redMaterial = new SCNMaterial();
                    redMaterial.Diffuse.Contents = UIColor.Red;

                    node.Geometry.Materials = new SCNMaterial[] { redMaterial };
                }
                Action<SCNNode> transform = null;
                SCNNode model;
                SCNVector3 scale = new SCNVector3(0.1f, 0.1f, 0.1f);
                SCNVector3 start = new SCNVector3(-2f, 0f, -1f);
                SCNVector3 offset = new SCNVector3(1, 0, 0);
                int index = 0;
                ObjectNode n = new ObjectNode("art.scnassets/aircraft.dae");
                transform = (m) =>
                {
                    m.Position = SCNVector3.Add(start, offset);
                    m.Scale = scale;
                };
                model = n.CreateInstance(transform);
                regularView.Scene.RootNode.AddChildNode(model);
                aircraft = model;

                
                var lineG = new SCNCylinder();
                lineG.Radius = 0.2f;
                lineG.Height = 1f;
                var line = new SCNNode();
                line.Geometry = lineG;
                line.Position = new SCNVector3(1, 0, 1);
            }
        }
    }

    public class CubeNode : SCNNode
    {
        public CubeNode(float size, UIColor color)
        {
            var rootNode = new SCNNode
            {
                Geometry = CreateGeometry(size, color)
            };
            AddChildNode(rootNode);
        }
        private static SCNGeometry CreateGeometry(float size, UIColor color)
        {
            var material = new SCNMaterial();
            material.Diffuse.Contents = color;

            var geometry = SCNBox.Create(size, size, size, 0);
            geometry.Materials = new[] { material };
            return geometry;
        }
    }

    public class ObjectNode
    {
        private string path;
        private static int _id = 0;

        public ObjectNode(string path)
        {
            this.path = path;
        }
        public SCNNode CreateInstance(Action<SCNNode> transformModel = null)
        {
            var sceneFromFile = SCNScene.FromFile(path);

            var model = sceneFromFile.RootNode.FindChildNode("Plane", true);

            transformModel?.Invoke(model);
            /*
            model.Scale = new SCNVector3(0.1f, 0.1f, 0.1f);

            var matrix = SCNMatrix4.Identity;
            float a = (float)Math.Sin(Math.PI / 4);
            matrix.SetXAxis(new SCNVector3(a, -a, 0));
            matrix.SetYAxis(new SCNVector3(0, 1, 0));
            matrix.SetZAxis(new SCNVector3(a, 0, a));
            model.Transform = matrix;

            //model.Transform = SCNMatrix4.CreateRotationY((float)(-Math.PI / 4));
            //model.Position = new SCNVector3(0, 0, -1);
            */
            int id = _id++;
            string s = "Model id: " + id + "\n" +
                        $"Right: {model.WorldRight} \n" +
                        $"Up: {model.WorldUp} \n" +
                        $"Forward: {model.WorldFront} \n" +
                        $"Translate: {model.Position} \n";
            Console.WriteLine(s);


            //float angle = (float)(-Math.PI / 2);
            //model.EulerAngles = new SCNVector3(0, 0, 0);

            //model.Opacity = 0;

            //model.RunAction(SCNAction.FadeIn(0.75));
            var material = new SCNMaterial();
            material.Diffuse.Contents = new UIImage("art.scnassets/aircraft.png");
            model.Geometry.FirstMaterial = material;

            return model;
            //AddChildNode(model);
        }
    }
}