﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UIKit;
using Xamarin.Essentials;

namespace XamarinARKit.Shared.Geometry
{
    /// <summary>
    /// Helper methods for Geolocation calculations.
    /// </summary>
    public class GeoHelpers
    {
        /// <summary>
        /// Calculation of bearing from internet.
        /// <para>This is platform independent implementation.</para>
        /// </summary>
        /// <param name="l1"></param>
        /// <param name="l2"></param>
        /// <returns></returns>
        /// <seealso cref="https://www.igismap.com/formula-to-find-bearing-or-heading-angle-between-two-points-latitude-longitude/"/>
        /// <seealso cref="https://towardsdatascience.com/calculating-the-bearing-between-two-geospatial-coordinates-66203f57e4b4"/>
        /// <see cref="https://www.movable-type.co.uk/scripts/latlong.html"/>
        public static float GetBearingDeg(Location l1, Location l2)
        {
            double lat1 = l1.Latitude;
            double lon1 = l1.Longitude;
            double lat2 = l2.Latitude;
            double lon2 = l2.Longitude;

            Func<double, double> toRad = (d) => d * Math.PI / 180;

            lat1 = toRad(lat1);
            lon1 = toRad(lon1);
            lat2 = toRad(lat2);
            lon2 = toRad(lon2);

            double x = Math.Cos(lat2) * Math.Sin(lon2 - lon1);
            double y = Math.Cos(lat1) * Math.Sin(lat2) - Math.Sin(lat1) * Math.Cos(lat2) * Math.Cos(lon2 - lon1);

            double beta = Math.Atan2(x, y);

            beta = beta * 180 / Math.PI;
            return (float)beta;
        }

        /// <summary>
        /// Calculate distance in meters, between 2 points in GPS coordinates.
        /// </summary>
        /// <param name="l1"></param>
        /// <param name="l2"></param>
        /// <returns></returns>
        public static float GetDistance(Location l1, Location l2)
        {
            double d = l1.CalculateDistance(l2, DistanceUnits.Kilometers);
            return (float)(d * 1000f);
        }
    }
}