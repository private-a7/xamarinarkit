﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;

namespace ARTest.Geometry
{
    //TODO Implement point.

    /// <summary>
    /// Vector in 3d space.
    /// </summary>
    /// <remarks>
    ///     Implemented as Facade to System.Numerics.Vector3.
    ///     Intended to rewrite completely.
    /// </remarks>
    public struct AVector3
    {
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }
        public float Length { get; private set; }

        public AVector3(float[] xyz)
        {
            X = xyz[0];
            Y = xyz[1];
            Z = xyz[2];
            Length = new Vector3(X, Y, Z).Length();
        }
        public AVector3(float x, float y, float z = 0)
        {
            X = x;
            Y = y;
            Z = z;
            Length = new Vector3(X, Y, Z).Length();
        }
        public void SetHeight(float y = 0)
        {
            Y = y;
            Length = this.ToVector3().Length();
        }

        public float[] ToArray() => new[] { X, Y, Z };

        /// <summary>
        /// Returns unit vector (Length is 1).
        /// </summary>
        /// <returns></returns>
        public AVector3 GetUnit() => Vector3.Normalize(this).ToAVector3();

        public AVector3 Add(AVector3 v)
            => Vector3.Add(this, v).ToAVector3();

        public AVector3 Multiply(float d)
            => Vector3.Multiply(this, d).ToAVector3();

        public AVector3 CrossProduct(AVector3 v)
            => Vector3.Cross(this, v).ToAVector3();

        public float DotProduct(AVector3 v)
            => Vector3.Dot(this, v);

        public AVector3 Negate()
            => Vector3.Negate(this).ToAVector3();

        /// <summary>
        /// Distance between vectors.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public float DistanceTo(AVector3 v)
            => Vector3.Distance(this, v);

        /// <summary>
        /// Squared distance to.
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public float DistanceSquaredTo(AVector3 v)
            => Vector3.DistanceSquared(this, v);

        public static AVector3 XAxis => new AVector3(1, 0, 0);
        public static AVector3 YAxis => new AVector3(0, 1, 0);
        public static AVector3 ZAxis => new AVector3(0, 0, 1);

        public static AVector3 operator *(AVector3 v, float scalar)
            => v.Multiply(scalar);


        /// <summary>
        /// Vector product of vectors.
        /// </summary>
        public static AVector3 operator *(AVector3 v1, AVector3 v2)
            => v1.CrossProduct(v2);
        /// <summary>
        /// Vector product of vectors.
        /// </summary>
        public static AVector3 operator *(Vector3 v1, AVector3 v2)
            => v1.ToAVector3().CrossProduct(v2);
        /// <summary>
        /// Vector product of vectors.
        /// </summary>
        public static AVector3 operator *(AVector3 v1, Vector3 v2)
            => v1.CrossProduct(v2.ToAVector3());


        /// <summary>
        /// Sum of vectors.
        /// </summary>
        public static AVector3 operator +(AVector3 v1, AVector3 v2)
            => v1.Add(v2);
        /// <summary>
        /// Sum of vectors.
        /// </summary>
        public static AVector3 operator +(AVector3 v1, Vector3 v2)
            => v1.Add(v2.ToAVector3());
        /// <summary>
        /// Sum of vectors.
        /// </summary>
        public static AVector3 operator +(Vector3 v1, AVector3 v2)
            => v1.ToAVector3().Add(v2);


        public static implicit operator Vector3(AVector3 v)
            => v.ToVector3();

        public override string ToString()
        {
            Func<float, string> toString = (d) => d.ToString("0.00");
            return $"({toString(X)}, {toString(Y)}, {toString(Z)})";
        }
    }

    /// <summary>
    /// 4x4 matrix in 3d system.
    /// </summary>
    public struct AMatrix3
    {
        /// <summary>
        /// Matrix (4x4) fields are arranged:
        /// <para>(0,0), (0,1), (0,2) (0,3)</para>
        /// <para>(1,0), (1,1), (1,2) (1,3)</para>
        /// <para>(2,0), (2,1), (2,2) (2,3)</para>
        /// <para>(3,0), (3,1), (3,2) (3,3)</para>
        /// </summary>
        public float[,] Table { get; private set; }
        /// <summary>
        /// Table flaten out by rows (row1,row2...)
        /// <para>(0,0),(0,1),(0,2),(0,3),(1,0)...</para>
        /// </summary>
        public float[] Array { get; private set; }

        public AMatrix3(float[] matrix)
        {
            Array = matrix.ToArray();
            Table = new float[4, 4];
            int i = 0;
            int j = 0;
            for (int k = 0; k < 16; k++)
            {
                float p = matrix[k];
                Table[i, j] = p;
                j++;
                if (j > 3)
                {
                    i++;
                    j = 0;
                }
            }
        }

    }

    public static class GeometryExtensions
    {
        public static AVector3 ToVector(this float[] xyz) => new AVector3(xyz);

        /// <summary>
        /// Converts AVector3 to System.Numerics.Vector3.
        /// </summary>
        public static Vector3 ToVector3(this AVector3 v) => new Vector3(v.X, v.Y, v.Z);

        /// <summary>
        /// Converts System.Numerics.Vector3 to AVector3.
        /// </summary>
        public static AVector3 ToAVector3(this Vector3 v) => new AVector3(v.X, v.Y, v.Z);


        public static Matrix4x4 ToMatrix(this float[] matrix44)
        {
            var m = matrix44;
            int i0, i1, i2, i3;
            i0 = 0;
            i1 = 4;
            i2 = 8;
            i3 = 12;
            var matrix = new Matrix4x4
                (
                    m[i0 + 0], m[i0 + 1], m[i0 + 2], m[i0 + 3],
                    m[i1 + 0], m[i1 + 1], m[i1 + 2], m[i1 + 3],
                    m[i2 + 0], m[i2 + 1], m[i2 + 2], m[i2 + 3],
                    m[i3 + 0], m[i3 + 1], m[i3 + 2], m[i3 + 3]
                );
            return matrix;
        }
        /// <summary>
        /// Converts matrix 4x4 to array (concatenated rows) of 16.
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static float[] ToArray(this Matrix4x4 m)
        {
            float[] array = new float[16]
            {
                m.M11, m.M12, m.M13, m.M14,
                m.M21, m.M22, m.M23, m.M24,
                m.M31, m.M32, m.M33, m.M34,
                m.M41, m.M42, m.M43, m.M44
            };
            return array;
        }
    }
}
