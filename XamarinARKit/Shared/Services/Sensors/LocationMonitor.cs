﻿using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UIKit;
using Xamarin.Essentials;

namespace XamarinARKit.Shared.Services.Sensors
{
    public class LocationMonitor
    {
        public static Location LastLocation { get; set; } = new Location(44f, 22f);
        private static bool running = false;
        public static async void StartTracking()
        {
            running = true;
            //var t = new Task(async () =>
            int counter = 100;
            //while (running)
            {
                counter++;
                // Check every 1000ms.
                if (counter > 10)
                {
                    counter = 0;
                    var l = await Geolocation.GetLastKnownLocationAsync();
                    if (l == null)
                        l = await Geolocation.GetLocationAsync();
                    if (l != null)
                        LastLocation = l;
                }
                await Task.Delay(100);
            }
        }
        public static void StopTracking()
        {
            running = false;
        }
    }
}