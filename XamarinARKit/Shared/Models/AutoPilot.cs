﻿using ARTest.Geometry;
using ARTest.Services.Sensors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ARTest.Models
{

    public class AutoPilot
    {
        public Aircraft Aircraft { get; } = new Aircraft();

        private float sign = 1;

        public AutoPilot()
        {
            Aircraft.SetPosition(new AVector3(0, 0f, 0));
        }
        public void MakeStep()
        {
            Aircraft.SetRoll(OrientationMonitor.BankAngleRad);
            //Aircraft.SetPosition(new AVector3(1, 0, -1));
            Aircraft.Move();
            return;
            Aircraft.SetDirection(new AVector3(1, 0, -1), 45);
            return;
            var t = new double[]
            {
                0,  0, -1,  0,
                0,  -1,  0,  0,
                1,  0,  0,  0,
                0,  0,  0,  1
            }.Select(x => (float)x).ToArray();
            Aircraft.SetTransform(t);

            return;
            return;
            /*
            if (sign > 0)
            {
                if (sum > 2)
                    sign = -1;                    
            }
            else if (sign < 0)
            {
                if (sum < -2)
                    sign = 1;
            }*/
        }
    }
}
