﻿using ARTest;
using ARTest.Geometry;
using ARTest.Services.Sensors;
using Foundation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using UIKit;
using Xamarin.Essentials;
using XamarinARKit.Shared.Geometry;
using XamarinARKit.Shared.Services.Sensors;

namespace XamarinARKit.Models
{
    public class CoordinateConverter
    {
        /// <summary>
        /// Calculates position of target in Augmented World, for it's given GPS
        /// coordinates. Calculation is relative to current properties of phone.
        /// </summary>
        /// <param name="l"></param>
        /// <param name="cameraTransform">
        /// Current position of the phone's camera in AW.
        /// Fetched from camera's display oriented pose.</param>
        /// <returns>Vector from (0,0,0) to target (in augmented world).</returns>
        public static Vector3 CalculateARPositionOfTarget(Location l, Matrix4x4 cameraTransform)
        {
            float north = OrientationMonitor.MagneticNorthDeg;
            var m = cameraTransform;
            var direction = m.GetVectorZ().ToVector3() * -1;
            direction = new Vector3(direction.X, 0, direction.Z);
            direction = Vector3.Normalize(direction);
            var offset = m.GetVectorTranslation();

            Location current = LocationMonitor.LastLocation;
            var bearing = GeoHelpers.GetBearingDeg(current, l);
            var distance = GeoHelpers.GetDistance(current, l);

            var deltaAngleDeg = north - bearing;
            var matrix = Matrix4x4.CreateRotationY((float)(deltaAngleDeg * Math.PI / 180));
            var v = Vector3.Transform(direction, matrix);
            v = v * (float)distance;
            var position = offset + v;

            return position;
        }

        /// <summary>
        /// Coordinate system is matched to camera's - direction of camera in RW is 
        /// same as -zAxis in AW.
        /// </summary>
        /// <param name="l"></param>
        /// <returns></returns>
        public static Vector3 CalculateARPositionOfTarget(MyLocation l, StringBuilder builder = null)
        {
            builder.AppendLine("-----------------");
            builder.AppendLine($"Location: {l.Name}");
            float north = OrientationMonitor.MagneticNorthDeg;
            //var m = cameraTransform;
            var direction = new Vector3(0, 0, -1);// m.GetVectorZ().ToVector3() * -1;
            direction = new Vector3(direction.X, 0, direction.Z);
            direction = Vector3.Normalize(direction);
            //var offset = m.GetVectorTranslation();

            Location current = LocationMonitor.LastLocation;
            var bearing = GeoHelpers.GetBearingDeg(current, l.Location);
            var distance = GeoHelpers.GetDistance(current, l.Location) * l.DistanceScale;

            var deltaAngleDeg = north - bearing;
            while (deltaAngleDeg > 360)
                deltaAngleDeg -= 360;
            while (deltaAngleDeg < 0)
                deltaAngleDeg += 360;
            string brString = $"{bearing.ToString("0.00").PadLeft(8)} / {deltaAngleDeg.ToString("0.00").PadLeft(8)}";
            builder.AppendLine($"Bearing (N/d): {brString}");
            builder.AppendLine($"Distance: {distance:0.00}");

            var matrix = Matrix4x4.CreateRotationY((float)(deltaAngleDeg * Math.PI / 180));
            var v = Vector3.Transform(direction, matrix);
            v = v * (float)distance;
            var position = v;// offset + v;
            builder.AppendLine($"Position: x={v.X:0.00}, y={v.Y:0.00}, z={v.Z:0.00}");

            return position;
        }
    }
}